def orientation(points):
    return (points[2][1] - points[0][1]) * (points[1][0] - points[0][0]) > (points[1][1] - points[0][1]) * (points[2][0] - points[0][0])

def intersect(seg1, seg2):
    return orientation((seg1[0], seg2[0], seg2[1])) != orientation((seg1[1], seg2[0], seg2[1])) and orientation((seg1[0], seg1[1], seg2[0])) != orientation((seg1[0], seg1[1], seg2[1]))
