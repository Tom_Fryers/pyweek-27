Sixth Gear
==========

Running the Game
----------------
To play Sixth Gear, simply enter `python main.py` into the terminal, while in
this directory. Instructions for how to play are availably in the game, or in
instructions.txt if you'd rather read them seperately.

Dependencies
------------
* Python 3.6+
* Pygame 1.9.4

Credits
-------
I, LeopardShark, wrote all of the code for the game, and created all of the
artwork. The tracks were edited from SVGs of the tracks from their individual
Wikipedia pages:
[Austria](https://commons.wikimedia.org/wiki/File:Circuit_Red_Bull_Ring.svg), 
[Italy](https://commons.wikimedia.org/wiki/File:Monza_track_map.svg) and
[Monaco](https://commons.wikimedia.org/wiki/File:Monte_Carlo_Formula_1_track_map.svg).

The font is Oxanium, available here:
https://sev.dev/fonts/oxanium/

The music is all from Kevin MacLeod:

* "Le Grand Chase" Kevin MacLeod (incompetech.com)

   Licensed under Creative Commons: By Attribution 3.0 License

   http://creativecommons.org/licenses/by/3.0/

* "Special Spotlight" Kevin MacLeod (incompetech.com)

   Licensed under Creative Commons: By Attribution 3.0 License

   http://creativecommons.org/licenses/by/3.0/

* "Take a Chance" Kevin MacLeod (incompetech.com)

   Licensed under Creative Commons: By Attribution 3.0 License

   http://creativecommons.org/licenses/by/3.0/

The menu click sound is edited from [this](https://freesound.org/people/lebaston100/sounds/192272/).

This game was created for [Pyweek](https://pyweek.org/), a fun competition in
which entrants create a game in a single week, using Python.

Miscellaneous Information
-------------------------
The tracks in the game are fairly accurate representations of real tracks. They
are the Red Bull Ring, in Austria, the Autodromo Nazionale Monza, in Italy, and
the Circuit de Monaco, in Monaco. The car is based on the Chevrolet Corvette
Z06, but I made some changes for game balance reasons. The target time for the
Austra circuit is the lap record, but the others are arbitrary because the real
records seemed too difficult.
