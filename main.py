import time
import math

import pygame
from pygame.locals import *

import ui
import game

import constants
WIDTH = constants.WIDTH
HEIGHT = constants.HEIGHT

#import cProfile
#import re

def main():
    FONTFO = pygame.font.Font('data/oxanium/ttf/Oxanium-Regular.ttf', 60)
    LOADING = FONTFO.render('Loading...', 1, (69, 63, 69))
    S.fill((188, 162, 35))
    S.blit(LOADING, ((WIDTH - LOADING.get_width()) / 2, 400))
    pygame.display.update()

    # Main menu
    FONTFI = pygame.font.Font('data/oxanium/ttf/Oxanium-ExtraBold.ttf', 120)
    TITLE = FONTFI.render('Sixth Gear', 1, (0, 0, 0))

    # Load the car image
    carpic = pygame.image.load('data/car.png').convert_alpha()

    main_menu = ui.Menu(['Level Select', 'Instructions', 'Change Units', 'Quit'], pygame.font.Font('data/oxanium/ttf/Oxanium-Regular.ttf', 80), 90, COLOURS, (200, 300))

    ext = False
    click = False
    t = 0
    menuclock = pygame.time.Clock()
    while not ext:
        t += 1
        gonext = False
        for event in pygame.event.get():
            if event.type == QUIT:
                ext = True
                break
            elif event.type == MOUSEBUTTONDOWN:
                click = True
            elif event.type == MOUSEBUTTONUP:
                click = False
                gonext = True
        mp = pygame.mouse.get_pos()
        item = main_menu.get_click(mp)

        if gonext:
            CLICK.play()
            if item == 'Level Select':
                if level_select(LOADING):
                    ext = True
                    break
                click = False
                pygame.event.get()
            elif item == 'Instructions':
                instructions()
            elif item == 'Change Units':
                settings()
            elif item == 'Quit':
                ext = True
                break
        S.fill((188, 162, 35))
        pygame.draw.circle(S, (69, 63, 69), (900, 500), 150)

        rotcar = pygame.transform.rotozoom(carpic, t, 1)
        ti = time.time()
        S.blit(rotcar, (900 - rotcar.get_width() / 2, 500 - rotcar.get_height() / 2))
        S.blit(TITLE, ((WIDTH - TITLE.get_width()) / 2, 50))
        main_menu.draw(S, click)
        pygame.display.update()
        menuclock.tick(60)

def level_select(LOADING):
    LEVELS = 3
    with open('leveldata.csv', 'r') as f:
        leveldata = [x.split(',') for x in f.read().split('\n')]
    with open('scores.csv', 'r') as f:
        scores = f.read().split('\n')

    FONTFH = pygame.font.Font('data/oxanium/ttf/Oxanium-Bold.ttf', 90)
    HEADING = FONTFH.render('Choose a level', 1, (69, 63, 69))
    
    FONTFC = pygame.font.Font('data/oxanium/ttf/Oxanium-Bold.ttf', 60)
    COLHEAD = FONTFC.render('Your best   Target    Track', 1, (69, 63, 69))

    leveltext = []
    for level in range(1, LEVELS + 1, 1):
        leveltext.append(scores[level] + ' / ' + leveldata[level][1] + ' ' + leveldata[level][2])
    level_menu = ui.Menu(leveltext + ['Back'], pygame.font.Font('data/oxanium/ttf/Oxanium-Regular.ttf', 70), 90, COLOURS, (200, 300))

    ext = False
    back = False
    click = False
    while not ext:
        gonext = False
        for event in pygame.event.get():
            if event.type == QUIT:
                ext = True
                break
            elif event.type == MOUSEBUTTONDOWN:
                click = True
            elif event.type == MOUSEBUTTONUP:
                click = False
                gonext = True
        mp = pygame.mouse.get_pos()
        item = level_menu.get_click(mp)

        if gonext:
            CLICK.play()
            try:
                level = leveltext.index(item) + 1
            except ValueError:
                back = True
            break
        S.fill((188, 162, 35))
        S.blit(HEADING, ((WIDTH - HEADING.get_width()) / 2, 80))
        S.blit(COLHEAD, ((200, 220)))
        level_menu.draw(S, click)
        pygame.display.update()
    if not (ext or back):
        game.play_level(S, level, leveldata[level][2], scores[level], leveldata[level][1], int(leveldata[level][0]), LOADING)
        pygame.mixer.music.load('data/Special Spotlight.mp3')
        pygame.mixer.music.play(-1)
    return ext


def settings():

    FONTFH = pygame.font.Font('data/oxanium/ttf/Oxanium-Bold.ttf', 90)
    HEADING = FONTFH.render('Choose a speed unit', 1, (69, 63, 69))

    settings_menu = ui.Menu(['mph', 'km/h', 'm/s', 'Back'], pygame.font.Font('data/oxanium/ttf/Oxanium-Regular.ttf', 70), 90, COLOURS, (200, 300))

    ext = False
    back = False
    click = False
    while not ext:
        gonext = False
        for event in pygame.event.get():
            if event.type == QUIT:
                ext = True
                break
            elif event.type == MOUSEBUTTONDOWN:
                click = True
            elif event.type == MOUSEBUTTONUP:
                click = False
                gonext = True
        mp = pygame.mouse.get_pos()
        item = settings_menu.get_click(mp)

        if gonext:
            CLICK.play()
            if item == 'Back':
                back = True
            else:
                with open('units.txt', 'w') as f:
                    f.write(item)
            break
        S.fill((188, 162, 35))
        S.blit(HEADING, ((WIDTH - HEADING.get_width()) / 2, 80))
        settings_menu.draw(S, click)
        pygame.display.update()


def instructions():
    with open('instructions.txt', 'r') as f:
        image = ui.text_block(f.read(), pygame.font.Font('data/oxanium/ttf/Oxanium-Regular.ttf', 28), (69, 63, 69), 33)

    ext = False
    while not ext:
        S.fill((188, 162, 35))
        S.blit(image, (40, 20))
        pygame.display.update()
        for event in pygame.event.get():
            if event.type in {MOUSEBUTTONUP, QUIT, KEYDOWN}:
                CLICK.play()
                ext = True
                break

if __name__ == '__main__':
    
    pygame.mixer.pre_init(44100, -16, 2, 1024)
    pygame.init()
    pygame.mixer.quit()
    pygame.mixer.init(44100, -16, 2, 1024)

    pygame.mixer.music.load('data/Special Spotlight.mp3')
    pygame.mixer.music.play(-1)
    
    CLICK = pygame.mixer.Sound('data/click2.wav')
    
    S = pygame.display.set_mode((WIDTH, HEIGHT))
    pygame.display.set_caption('Sixth Gear')
    COLOURS = [(69, 63, 69), (40, 40, 40), (43, 52, 217)]
    main()
    #cProfile.run('main()', sort='cumtime')
