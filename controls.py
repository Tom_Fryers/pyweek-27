from pygame.locals import *

keys = {'accelerate': K_UP,
        'brake': K_DOWN,
        'left': K_LEFT,
        'right': K_RIGHT,
        'shift up': K_w,
        'shift down': K_s,
        'restart': K_r,
        }
